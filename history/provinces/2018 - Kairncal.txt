# No previous file for Kairncal
owner = H05
controller = H05
add_core = H05
culture = selphereg
religion = eordellon
capital = "Kairncal"

hre = no

base_tax = 4
base_production = 5
base_manpower = 3

trade_goods = gems

native_size = 14
native_ferocity = 6
native_hostileness = 6
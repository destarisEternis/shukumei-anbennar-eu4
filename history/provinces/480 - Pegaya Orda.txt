# No previous file for Pegaya Orda
owner = F09
controller = F09
add_core = F09
culture = sandfang_gnoll
religion = xhazobkult

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = livestock

capital = ""

is_city = yes


discovered_by = tech_salahadesi
discovered_by = tech_gnollish
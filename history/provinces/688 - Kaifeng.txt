# No previous file for Kaifeng

owner = F28
controller = F28
add_core = F28
culture = sandfang_gnoll
religion = xhazobkult

hre = no

base_tax = 3
base_production = 3
base_manpower = 4

trade_goods = slaves

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_bulwari

#421 - Shirvan

capital = ""
trade_goods = unknown
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
native_size = 0
native_ferocity = 0
native_hostileness = 0

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
# No previous file for Dinesck
owner = Z25
controller = Z25
add_core = Z25
culture = blue_reachman
religion = regent_court

hre = no

base_tax = 4
base_production = 4
base_manpower = 3

trade_goods = fur

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_kobold
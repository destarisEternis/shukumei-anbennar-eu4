# No previous file for Mosquito
owner = A33
controller = A33
add_core = A33
culture = vernman
religion = regent_court

hre = yes

base_tax = 3
base_production = 4
base_manpower = 2

trade_goods = copper

capital = ""

is_city = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_gnollish
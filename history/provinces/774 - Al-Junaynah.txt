#774

owner = B46
controller = B46
add_core = B46
culture = common_goblin
religion = goblinic_shamanism


base_tax = 3
base_production = 2
base_manpower = 2

trade_goods = livestock
center_of_trade = 1

capital = ""

is_city = yes

native_size = 78
native_ferocity = 5
native_hostileness = 10


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
discovered_by = tech_goblin
discovered_by = tech_bulwari
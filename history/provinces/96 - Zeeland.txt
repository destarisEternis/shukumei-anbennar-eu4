#96 - Zeeland |

owner = A18
controller = A18
add_core = A18
culture = low_lorentish
religion = regent_court

hre = no

base_tax = 2
base_production = 4
base_manpower = 3

trade_goods = livestock

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish


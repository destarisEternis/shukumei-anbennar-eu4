# No previous file for Karta
owner = F25
controller = F25
add_core = F25
culture = zanite
religion = bulwari_sun_cult

hre = no

base_tax = 3
base_production = 2
base_manpower = 2

trade_goods = grain

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari
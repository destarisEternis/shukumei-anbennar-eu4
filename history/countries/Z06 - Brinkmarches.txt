government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
primary_culture = arannese
religion = regent_court
technology_group = tech_cannorian
capital = 322
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1400.1.2 = { set_country_flag = is_a_county }

1403.1.4 = {
	monarch = {
		name = "Walter V"
		dynasty = "Brinkwick"
		birth_date = 1378.12.2
		adm = 0
		dip = 1
		mil = 1
	}
}

1422.1.1 = { set_country_flag = lilac_wars_moon_party }
government = republic
add_government_reform = free_city
government_rank = 1
mercantilism = 25
primary_culture = wexonard
religion = regent_court
technology_group = tech_cannorian
capital = 294 
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1440.1.1 = {
	monarch = {
		name = "City Council"
		adm = 2
		dip = 2
		mil = 2
		regent = yes
	}
}
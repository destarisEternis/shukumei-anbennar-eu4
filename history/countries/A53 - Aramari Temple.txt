government = theocracy
add_government_reform = leading_clergy_reform
government_rank = 1
primary_culture = crownsman
religion = regent_court
technology_group = tech_cannorian
capital = 249

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1437.3.1 = {
	monarch = {
		name = "Marion the Transmuter"
		birth_date = 1376.9.3
		adm = 6
		dip = 0
		mil = 0
	}
}
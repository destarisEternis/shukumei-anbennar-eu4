#Country Name: Please see filename.

graphical_culture = southamericagfx

color = { 22  105  163 }

revolutionary_colors = { 221  18  77 }

historical_idea_groups = {
	defensive_ideas
	economic_ideas
	expansion_ideas
	religious_ideas
	innovativeness_ideas
	trade_ideas
	quality_ideas
}


historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	native_indian_horsemen
	huron_arquebusier
	commanche_swarm
	native_indian_mountain_warfare
	american_western_franchise_warfare
}

monarch_names = {

	#Generic Aelantiri Names
	"Adral #0" = 1
	"Aldaran #0" = 1
	"Aldaron #0" = 1
	"Allaran #0" = 1
	"Allor #0" = 1
	"Alwaron #0" = 1
	"Andrall #0" = 1
	"Arantir #0" = 1
	"Ardor #0" = 1
	"Ardoran #0" = 1
	"Arfall #0" = 1
	"Arlas #0" = 1
	"Artor #0" = 1
	"Danaran #0" = 1
	"Darran #0" = 1
	"Dharastor #0" = 1
	"Dorndor #0" = 1
	"Eboran #0" = 1
	"Elanal #0" = 1
	"Eldas #0" = 1
	"Elran #0" = 1
	"Elrand #0" = 1
	"Elthar #0" = 1
	"Elthor #0" = 1
	"Erlan #0" = 1
	"Ernedal #0" = 1
	"Erran #0" = 1
	"Ewandil #0" = 1
	"Filnar #0" = 1
	"Finnoran #0" = 1
	"Galhor #0" = 1
	"Galinal #0" = 1
	"Galinaldor #0" = 1
	"Galind #0" = 1
	"Galmon #0" = 1
	"Ibban #0" = 1
	"Iwan #0" = 1
	"Iwandal #0" = 1
	"Iwandar #0" = 1
	"Jahainar #0" = 1
	"Jahair #0" = 1
	"Kaladal #0" = 1
	"Kalador #0" = 1
	"Kaladoran #0" = 1
	"Kalindan #0" = 1
	"Kalrod #0" = 1
	"Kalsandel #0" = 1
	"Kalsander #0" = 1
	"Kamnar #0" = 1
	"Kamnaran #0" = 1
	"Kamnaril #0" = 1
	"Karod #0" = 1
	"Manaron #0" = 1
	"Nestrin #0" = 1
	"Pelodir #0" = 1
	"Serron #0" = 1
	"Serrondal #0" = 1
	"Serrondar #0" = 1
	"Talanor #0" = 1
	"Talar #0" = 1
	"Talaran #0" = 1
	"Talnial #0" = 1
	"Tarendal #0" = 1
	"Tarendar #0" = 1
	"Thalror #0" = 1
	"Trandil #0" = 1
	"Tretnor #0" = 1
	"Ultaran #0" = 1
	"Urian #0" = 1
	"Wacaran #0" = 1
	"Wandal #0" = 1
	"Warlor #0" = 1
	"Warmal #0" = 1
	"Warmallan #0" = 1
	"Warmar #0" = 1
	"Warnar #0" = 1

	"Allara #0" = -10
	"Allaranda #0" = -10
	"Allaratha #0" = -10
	"Allarwel #0" = -10
	"Amara #0" = -10
	"Aratha #0" = -10
	"Arathan #0" = -10
	"Dewena #0" = -10
	"Ebora #0" = -10
	"Ella #0" = -10
	"Elthora #0" = -10
	"Erlanda #0" = -10
	"Erlass #0" = -10
	"Fillnara #0" = -10
	"Gallind #0" = -10
	"Immarel #0" = -10
	"Ishera #0" = -10
	"Issarna #0" = -10
	"Issarnda #0" = -10
	"Istrallana #0" = -10
	"Iwarnda #0" = -10
	"Iwarndal #0" = -10
	"Jexassa #0" = -10
	"Kallasa #0" = -10
	"Kamnara #0" = -10
	"Keldora #0" = -10
	"Knarwen #0" = -10
	"Ladrana #0" = -10
	"Lallatha #0" = -10
	"Landel #0" = -10
	"Lanhara #0" = -10
	"Lanlora #0" = -10
	"Lasland #0" = -10
	"Marliath #0" = -10
	"Mitarel #0" = -10
	"Natala #0" = -10
	"Panorl #0" = -10
	"Sarndel #0" = -10
	"Sarrana #0" = -10
	"Sellsa #0" = -10
	"Serronda #0" = -10
	"Shara #0" = -10
	"Shawera #0" = -10
	"Talindela #0" = -10
	"Tanalla #0" = -10
	"Wallana #0" = -10
	"Warla #0" = -10
	"Warnna #0" = -10
	"Wehara #0" = -10
	"Weharran #0" = -10
	"Wesara #0" = -10
	"Zalara #0" = -10
	
}

leader_names = {

	#Geographic Titles
	"sal Arak" "sal Murdkath" "sal Deark" "sal Kairn" "sal Bagcatir" "sal Sidpar" "sal Einn" "sal Jhorg" "sal Raith" "sal Gemrad" "sal Elchos" "sal Domandrod"
	
	
}

ship_names = {
	Domandrod
	Eordand
	Arrag
	Fogar
	Sarmad
	Arakeprun
	Murdkather
	Kairncal
	Bagcatir
	Dearktir
	Gannag
	Darag
	Einnsag
	Raithtall
	Sidpar
	Jhorg
	Pelodir
	Gemrad
}	

army_names = {
	"$PROVINCE$ Aralt" "Fogar Aralt" "Eordellonian Aralt"
}

fleet_names = {
	"$PROVINCE$ Cablac" "Fogar Cablac"
}

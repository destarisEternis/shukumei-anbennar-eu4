#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 66  245  200 }

revolutionary_colors = { 66  245  200 }

historical_idea_groups = {
	economic_ideas
	offensive_ideas
	exploration_ideas
	defensive_ideas	
	administrative_ideas	
	maritime_ideas
	quality_ideas
	innovativeness_ideas
}

historical_units = {
	cannorian_1_medieval_infantry
	cannorian_1_medieval_knights
	cannorian_5_men_at_arms
	cannorian_9_pike_and_shot
	cannorian_10_gun_knights
	cannorian_12_thorn_formation
	cannorian_14_outriders
	cannorian_15_volley_infantry
	cannorian_18_reformed_outriders
	cannorian_19_thorn_formation2
	cannorian_23_line_infantry
	cannorian_26_carabiners
	cannorian_26_rosecoat_infantry
	cannorian_28_impulse_infantry
	cannorian_28_cuirassier
	cannorian_30_drill_infantry
}

monarch_names = {

	#Generic Anbennarian Name List
	"Adelar #0" = 1
	"Ademar #0" = 1
	"Adran #0" = 1
	"Adrian #0" = 1
	"Adrien #0" = 1
	"Alain #0" = 1
	"Alec #0" = 1
	"Albert #0" = 1
	"Aldred #0" = 1
	"Alfons #0" = 1
	"Alfred #0" = 1
	"Alos #0" = 1
	"Alvar #0" = 1
	"Andrel #0" = 1
	"Ardan #0" = 1
	"Ardor #0" = 1
	"Aril #0" = 1
	"Arman #0" = 1
	"Artorian #0" = 1
	"Artur #0" = 1
	"Aucan #0" = 1
	"Austyn #0" = 1
	"Avery #0" = 1
	"Awen #0" = 1
	"Borian #0" = 1
	"Brandon #0" = 1
	"Brayan #0" = 1
	"Brayden #0" = 1
	"Calas #0" = 1
	"Caylen #0" = 1
	"Cast�n #0" = 1
	"Cast�n #0" = 1
	"Cecill #0" = 1
	"Corin #0" = 1
	"Cristof #0" = 1
	"Daran #0" = 1
	"Darran #0" = 1
	"Davan #0" = 1
	"Denar #0" = 1
	"Dominic #0" = 1
	"Dustin #0" = 1
	"Edmund #0" = 1
	"Elran #0" = 1
	"Emil #0" = 1
	"Erel #0" = 1
	"Eren #0" = 1
	"Erlan #0" = 1
	"Evin #0" = 1
	"Frederic #0" = 1
	"Galin #0" = 1
	"Gelman #0" = 1
	"Kalas #0" = 1
	"Laurens #0" = 1
	"Lucian #0" = 1
	"Luciana #0" = 1
	"Marion #0" = 1
	"Maurise #0" = 1
	"Nara #0" = 1
	"Olor #0" = 1
	"Ot� #0" = 1
	"Re�n #0" = 1
	"Riann�n #0" = 1
	"Ricain #0" = 1
	"Ri�n #0" = 1
	"Robin #0" = 1
	"Rogier #0" = 1
	"Sandur #0" = 1
	"Taelar #0" = 1
	"Teagan #0" = 1
	"Thal #0" = 1
	"Thiren #0" = 1
	"Tom�s #0" = 1
	"Trian #0" = 1
	"Tristan #0" = 1
	"Trystan #0" = 1
	"Valen #0" = 1
	"Valeran #0" = 1
	"Varian #0" = 1
	"Varil #0" = 1
	"Varilor #0" = 1
	"Varion #0" = 1
	"Vernell #0" = 1
	"Vincen #0" = 1
	"Willam #0" = 1
	
	"Adeline #0" = -10
	"Adra #0" = -10
	"Alara #0" = -10
	"Aldresia #0" = -10
	"Alina #0" = -10
	"Alisanne #0" = -10
	"Amarien #0" = -10
	"Amina #0" = -10
	"Arabella #0" = -10
	"Aria #0" = -10
	"Athana #0" = -10
	"Adela #0" = -10
	"Aucanna #0" = -10
	"Bella #0" = -10
	"Calassa #0" = -10
	"Cast�nnia #0" = -10
	"Cast�na #0" = -10
	"Cecille #0" = -10
	"Cela #0" = -10
	"Celadora #0" = -10
	"Clarimonde #0" = -10
	"Constance #0" = -10
	"Cora #0" = -10
	"Coraline #0" = -10
	"Corina #0" = -10
	"Eil�s #0" = -10
	"Eil�sabet #0" = -10
	"El�anore #0" = -10
	"Emil�e #0" = -10
	"Erela #0" = -10
	"Erella #0" = -10
	"Galina #0" = -10
	"Galinda #0" = -10
	"Gis�le #0" = -10
	"Isabel #0" = -10
	"Isabella #0" = -10
	"Isobel #0" = -10
	"Kerstin #0" = -10
	"Laurenne #0" = -10
	"Lianne #0" = -10
	"Madal�in #0" = -10
	"Margery #0" = -10
	"Maria #0" = -10
	"Mariana #0" = -10
	"Marianna #0" = -10
	"Marianne #0" = -10
	"Marien #0" = -10
	"Marina #0" = -10
	"Re�nna #0" = -10
	"Sofia #0" = -10
	"Sofie #0" = -10
	"Sybille #0" = -10
	"Thalia #0" = -10
	"Valence #0" = -10
	"Varina #0" = -10
	"Varinna #0" = -10
	"Willamina #0" = -10
	"Lisolette #0" = -10
	
		#Generic Orc Name List
		"Ad�l #0" = 10
		"Amug #0" = 10
		"Azukor #0" = 10
		"Bagga #0" = 10
		"Bakbur #0" = 10
		"Bhagzud #0" = 10
		"Bolg #0" = 10
		"Borgu #0" = 10
		"Bork #0" = 10
		"Bral #0" = 10
		"Bras�r #0" = 10
		"Burggob #0" = 10
		"B�rog #0" = 10
		"B�bol #0" = 10
		"B�th #0" = 10
		"Conakk #0" = 10
		"Dhog #0" = 10
		"Dramm #0" = 10
		"Drazad #0" = 10
		"Drok #0" = 10
		"Dub #0" = 10
		"Dugz #0" = 10
		"Durg #0" = 10
		"D�sh #0" = 10
		"Flak #0" = 10
		"Flogg #0" = 10
		"Fubar #0" = 10
		"Gakk #0" = 10
		"Garl #0" = 10
		"Garm #0" = 10
		"Ghad #0" = 10
		"Ghash #0" = 10
		"Ghudd #0" = 10
		"Ghudrag #0" = 10
		"Ghur�d #0" = 10
		"Gh�m #0" = 10
		"Gluk #0" = 10
		"Gokk #0" = 10
		"Golm #0" = 10
		"Grisha #0" = 10
		"Grom #0" = 10
		"Gruk #0" = 10
		"Grumger #0" = 10
		"Gub� #0" = 10
		"Gukk #0" = 10
		"Gund #0" = 10
		"Hoglik #0" = 10
		"Hork #0" = 10
		"Horza #0" = 10
		"Hoshgrish #0" = 10
		"Hura #0" = 10
		"Ikom #0" = 10
		"Ishmoz #0" = 10
		"Kargath #0" = 10
		"Khagor #0" = 10
		"Khrosh #0" = 10
		"Kilrogg #0" = 10
		"Korgus #0" = 10
		"Koth #0" = 10
		"Krakhorn #0" = 10
		"Krimp #0" = 10
		"Kr�k #0" = 10
		"Kuga #0" = 10
		"Kurd #0" = 10
		"Larluk #0" = 10
		"Lorm #0" = 10
		"Luga #0" = 10
		"Lurb #0" = 10
		"Mabug #0" = 10
		"Maknag #0" = 10
		"Maku #0" = 10
		"Mogg #0" = 10
		"Mogru #0" = 10
		"Moz� #0" = 10
		"M�g #0" = 10
		"M�l #0" = 10
		"Muz� #0" = 10
		"Norsko #0" = 10
		"Nor�k #0" = 10
		"N�kra #0" = 10
		"N�rug #0" = 10
		"N�zu #0" = 10
		"Ogal #0" = 10
		"Ogb�r #0" = 10
		"Ogg #0" = 10
		"Okegg #0" = 10
		"Olrok #0" = 10
		"Orgrim #0" = 10
		"Orlog #0" = 10
		"Orok #0" = 10
		"Orok�g #0" = 10
		"Orthog #0" = 10
		"Prak #0" = 10
		"Pug #0" = 10
		"Pushkrimp #0" = 10
		"Ragdug #0" = 10
		"Rakk #0" = 10
		"Rash #0" = 10
		"Ratak #0" = 10
		"Regar #0" = 10
		"Ronk #0" = 10
		"Rug #0" = 10
		"Sam�r #0" = 10
		"Shador #0" = 10
		"Shaka #0" = 10
		"Sh�g #0" = 10
		"Skak #0" = 10
		"Skoth #0" = 10
		"Skun #0" = 10
		"Snafu #0" = 10
		"Stakug� #0" = 10
		"Takra #0" = 10
		"Tarz #0" = 10
		"Thrak #0" = 10
		"Thr�l #0" = 10
		"Tugog #0" = 10
		"Tuhorn #0" = 10
		"T�ka #0" = 10
		"Udd�g #0" = 10
		"Ugakuga #0" = 10
		"Ugol #0" = 10
		"Ukbuk #0" = 10
		"Urim #0" = 10
		"Urimguk #0" = 10
		"Ushak #0" = 10
		"Uz�l #0" = 10
		"Varbuk #0" = 10
		"Varok #0" = 10
		"Zathra #0" = 10
		"Zhorg #0" = 10
		"Zhuk #0" = 10
		"Zog #0" = 10
		"Zosh #0" = 10
		"Zugor #0" = 10
		"Zur #0" = 10
		"Z�b #0" = 10
		"Z�ka #0" = 10
		
		"Ahe #0" = -10
		"Ahza #0" = -10
		"Atu #0" = -10
		"Awa #0" = -10
		"A�dga #0" = -10
		"Bhiel #0" = -10
		"Borba #0" = -10
		"Bur� #0" = -10
		"Dhoulza #0" = -10
		"Etrega #0" = -10
		"Ghorza #0" = -10
		"Glasha #0" = -10
		"Grama #0" = -10
		"Grane #0" = -10
		"G�rona #0" = -10
		"Khaara #0" = -10
		"Lagba #0" = -10
		"Lamma #0" = -10
		"Lazga #0" = -10
		"Mewa #0" = -10
		"Muroga #0" = -10
		"Neevn�z #0" = -10
		"Ohdaka #0" = -10
		"Orbha #0" = -10
		"Or�ph #0" = -10
		"Ovga #0" = -10
		"Rerk�l #0" = -10
		"Rurti #0" = -10
		"Shel #0" = -10
		"Sheru #0" = -10
		"Tid� #0" = -10
		"T�tga #0" = -10
		"Ugl�im #0" = -10
		"Uroph #0" = -10
		"Vogaa #0" = -10
		"Voltga #0" = -10
		"Yagza #0" = -10
		"Zien #0" = -10
}

leader_names = {
	#Country Specific
	"of Ancard's Crossing" "of the Crossing" Axeman
	
	#Province Neighbours
	"of Dockbridge" "of Trianc�st" "of Erngrove" "of Ilvandet" "of Carneter" "of Throatport" "of Stoneview" "of Windtower" "of Gabelaire"
	
	#Country Neighbours
	"of Carneter" "of Tretun" "of Wesdam" "of Woodwell" "of Neckcliffe" "of Saloren" "of Anbennc�st"
	
	#Geographic Neighbours
	"of Lencenor" "of the Damesneck" "of Dameria" "of the Winebay"
	
	#Generic English names and occupational names
	Smith Youngheart Adamson Henryson Donaldson Cook Baker Rider Lampman Fisher Fischer Tanner Miller Banks Taylor Cobble Jones Davies Williams Roberts White Black Grey Gray Green Red Blue Edwards Wood Woods Clarke Davis Nicholson
	Priestley Campman Wright Parker Stewart Berry Porter Shepherd Freeman Potter Pitcher Alderman Arkwright Barber Baker Bailey Bender Bowyer Chapman Chandler Cheeseman Cook Cooper Cowell Crocker Cutler Earl Farmer 
	
	#Titles
	" " "'the Brown'"
	"'the Tall'" "'the Younger'" "'the Clever'" "'the Vigilant'" "'the Stern'" "'the Mighty'" "'Halfblood'" "'the Mudblooded'" "'the Kind'" "'the Able'" "'the Good'" "'the Fat'"
	
	#Occupational Surnames
	Skullgrinder "Mountain-Eater" Ironskin Ironhide Deadhide Hammerfist Hammerspike Spikefist Swordfist "Dead-eye" Bloodspear Blackheart Manhunter Pureblood "Bright Eyes" "Eagle Eyes" "Half-Tongue" "Grog-Maker" Deathmonger
	Swordmaster Warborn Stoneskin Warmonger Rockskull Skullblade Skullhorn Ogreborn Deadeye Longtooth Pigsnout Regehammer Crueldrum Stonefist Angerfang Steelsword Warpsteel Deathforge Cravenkiller Hellspitter Rocksnarler
	Deadbreath Bitteraxe Shadowfeast Bloodfeast Redfeast Bloodscream Wildmarcher Brokenhammer Brokentusk "No-eyes" Silentblade Rageblood Rageblade Doomhammer Hellscream Blackhand Doomhammer Saurfang Bladefist Bloodreaver
	Brightflesh Grandguard Armorskin Armorhide Steelhide Metalhide Metaljaw Sharpears Halfblood Ragechain Chaoschain Chaosblade Flameseeker Fireblood Battlefire Thunderstorm Thunderblood Thunderfang Axemaul Shadowblood
	
	
	#Titles
	"'the Stout'" "'the Rash'" "'the Deadly'" "'the Master'" "'the Slaver'" "'the Warlord'" "'the Relentless'" "'the Bold'" "'the Warrior'" "'the Death-bringer'" "'the Handsome'" "'the Greedy'" "'the Tyrant'" "'the Unbreakable'" "'the Great'"
	"'the Defiler'" "'the Dead'" "'the Poker'" "'the Strangler'" "'the Twisted'" "'the Punisher'" "'the Devourer'" "'the Gravewalker'" "'the Crafty'" "'the Smart'" "'the Unwashed'" "'the Vile'" "'the Whisperer'" "'the Seer'" "'the Wicked'" "'the Brander'"
	"'the Large'" "'the Tall'" "'the Fat'" "'the Small'" "'the Strong'" "'the Clever'" "'the Crusher'" "'the Halfblooded"
}

ship_names = {
	#Generic Cannorian
	Adamant Advantage Adventure Advice Answer Ardent Armada Arrogant Assistance Association Assurance Atlas Audacious 
	Bear Beaver "Black Galley" "Black Pinnace" "Black Prince" "Black Blood" Captain Centurion Coronation Courage Crocodile Crown
	Daisy Defence Defiance Devastation Diamond Director Dolphin Dragon Drake Dreadnaught Duke Duchess Eagle Elephant Excellent Exchange Expedition Experiment
	Falcon Fame Favourite Fellowship Fleetwood Flight Flirt Formidable Forrester Fortitude Fortune Gillyflower Globe "Golden Horse" "Golden Lion" "Golden Phoenix" Goliath Goodgrace Governor
	"Grand Mistress" "Great Bark" "Great Charity" "Great Galley" "Great Gift"  "Great Pinnace" "Great Zebra" "Green Dragon" Greyhound Griffin Guide "Half Moon" Hare Harpy Hawke Hazardous Heart
	Hero Hope "Hope Bark" "Hopeful Adventure" Humble Hunter Illustrious Impregnable Increase Indefatigable Inflexible Intrepid Invincible "Less Bark" "Less Pinnace" "Less Zebra"
	Lively Lizard Lion Magnanime Magnificent Majestic Makeshift Medusa Minotaur Moderate Monarch Moon Moor "New Bark" Ocean 
	Pansy Panther Parrot Porcupine Powerful President Prince "Prince Consort" "Prince Royal"
	Redoubtable Reformation Regent Renown Repulse Research Reserve Resistance Resolution Restoration Revenge 
	Salamander Sandwich Sapphire Satisfaction Seahorse Search Sheerness Speaker Speedwell Sphinx Splendid Sprite Stag Standard Stately Success Sunlight Sunbeam Superb Swan Sweepstake Swift Swiftsure
	Terrible Terror Thunderer Tiger Tremendous Trident Trinity Triumph Trusty
	Unicorn Union Unity Valiant Vanguard Venerable Vestal Veteran Victor Vindictive Virtue Violet
	Warrior Warspite "Young Prince" Zealous

	Survey Surveillance Cow Ox Bird Hawk Sovereign Emperor Count Lord Baron

	Knight Paladin Dragonslayer Cleric Rogue Fighter Ranger Sorcerer Wizard Warlock Monk Druid
	Strength Dexterity Constitution Intelligence Wisdom Charisma
	
	#Elven
	Lunetine Agraseina 
	
	#Regent Court Deities
	Castellos Dame "The Dame" Halanna Ysh Yshtralania Agradls Adean Esmaryal Ryala Edronias Falah Nerat Ara Minara Munas Moonsinger Nathalyne Begga Corin Balgar
	Uelos Drax'os
}

army_names = {
	"Varbuk's Freemen" "Army of $PROVINCE$"
}

fleet_names = {
	"Freemen Fleet" 
}
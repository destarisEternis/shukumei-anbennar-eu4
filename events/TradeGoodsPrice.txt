namespace = trade_goods_price

### Incense goes up 2.5 -> 3.75
country_event = {
	id = trade_goods_price.1
	title = trade_goods_price.1.t
	desc = trade_goods_price.1.d
	picture = ADVISOR_eventPicture
	
	fire_only_once = yes
	mean_time_to_happen = {
		years = 50
		
		modifier = {
			factor = 0.5
			
			ai = no
		}
	}
	
	trigger = {
		is_year = 1520
	}
	
	option = {
		name = trade_goods_price.1.a
		ai_chance = {factor = 100}
		
		change_price = {
			trade_goods = incense
			key = incense_rise_in_demand
			
			value = 0.5
			duration = -1
		}
		hidden_effect = {
			every_country = {
				limit = {
					ai = no
					NOT = { tag = ROOT }
				}
				country_event = { id = trade_goods_price.3 }
			}
		}
	}
}

### Incense magic boom 3.75 -> 5
country_event = {
	id = trade_goods_price.2
	title = trade_goods_price.2.t
	desc = trade_goods_price.2.d
	picture = ADVISOR_eventPicture
	
	fire_only_once = yes
	mean_time_to_happen = {
		years = 50
		
		modifier = {
			factor = 0.5
			
			ai = no
		}
	}
	
	trigger = {
		current_age = age_of_absolutism 
	}
	
	option = {
		name = trade_goods_price.2.a
		ai_chance = {factor = 100}
		
		change_price = {
			trade_goods = incense
			key = incense_magic_boom
			
			value = 0.5
			duration = -1
		}
		hidden_effect = {
			every_country = {
				limit = {
					ai = no
					NOT = { tag = ROOT }
				}
				country_event = { id = trade_goods_price.3 }
			}
		}
	}
}

country_event = {
	id = trade_goods_price.3
	title = trade_goods_price.3.t
	desc = trade_goods_price.3.d
	picture = ADVISOR_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	
	option = {
		name = trade_goods_price.3.a
		ai_chance = {factor = 100}
	}
}

### Age of Artificier Incense price 5 -> 3
country_event = {
	id = trade_goods_price.4
	title = trade_goods_price.4.t
	desc = trade_goods_price.4.d
	picture = BANKRUPTCY_eventPicture
	
	fire_only_once = yes
	mean_time_to_happen = {
		years = 25
		
		modifier = {
			factor = 0.5
			
			ai = no
		}
	}
	
	trigger = {
		current_age = age_of_revolutions
	}
	
	option = {
		name = trade_goods_price.2.a
		ai_chance = {factor = 100}
		
		change_price = {
			trade_goods = incense
			key = incense_artificier_age
			
			value = -0.8
			duration = -1
		}
		hidden_effect = {
			every_country = {
				limit = {
					ai = no
					NOT = { tag = ROOT }
				}
				country_event = { id = trade_goods_price.5 }
			}
		}
	}
}

country_event = {
	id = trade_goods_price.5
	title = trade_goods_price.5.t
	desc = trade_goods_price.5.d
	picture = BANKRUPTCY_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	
	option = {
		name = trade_goods_price.5.a
		ai_chance = {factor = 100}
	}
}


### Orcish Slave Trade. Slave Price 2 -> 3.75
country_event = {
	id = trade_goods_price.6
	title = trade_goods_price.6.t
	desc = trade_goods_price.6.d
	picture = SHIP_SAILING_eventPicture
	
	fire_only_once = yes
	mean_time_to_happen = {
		years = 25
		
		modifier = {
			factor = 0.5
			
			ai = no
		}
	}
	
	trigger = {
		is_year = 1600
		random_known_country = {
			capital_scope = {
				continent = europe
			}
			colony = 3
		}
		production_leader = {
			trade_goods = slaves
		}
	}
	
	option = {
		name = trade_goods_price.6.a
		ai_chance = {factor = 100}
		add_years_of_income = 2
		change_price = {
			trade_goods = slaves
			key = green_slave_demand
			
			value = 0.75
			duration = -1
		}
		hidden_effect = {
			every_country = {
				limit = {
					ai = no
					NOT = { tag = ROOT }
				}
				country_event = { id = trade_goods_price.7 }
			}
		}
	}
}

country_event = {
	id = trade_goods_price.7
	title = trade_goods_price.7.t
	desc = trade_goods_price.7.d
	picture = SHIP_SAILING_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	
	option = {
		name = trade_goods_price.7.a
		ai_chance = {factor = 100}
	}
}

### Price Dye Event. Noble use them to dye their hair. 4 -> 6

country_event = {
	id = trade_goods_price.8
	title = trade_goods_price.8.t
	desc = trade_goods_price.8.d
	picture = TRADE_GOODS_PLANTATION_GOODS_IN_WAREHOUSE_eventPicture
	
	fire_only_once = yes
	mean_time_to_happen = {
		years = 20
	}
	
	trigger = {
		current_age = age_of_absolutism
	}
	
	option = {
		name = trade_goods_price.8.a
		ai_chance = {factor = 100}
		
		change_price = {
			trade_goods = dyes
			key = noble_dye_hair
			
			value = 0.5
			duration = -1
		}
		hidden_effect = {
			set_global_flag = dye_price_up
			every_country = {
				limit = {
					ai = no
					NOT = { tag = ROOT }
				}
				country_event = { id = trade_goods_price.9 }
			}
		}
	}
}

country_event = {
	id = trade_goods_price.9
	title = trade_goods_price.9.t
	desc = trade_goods_price.9.d
	picture = TRADE_GOODS_PLANTATION_GOODS_IN_WAREHOUSE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	
	option = {
		name = trade_goods_price.9.a
		ai_chance = {factor = 100}
	}
}

### Add special modifier to Viswall(North and south) when the price of Dye goes up
country_event = {
	id = trade_goods_price.10
	title = trade_goods_price.10.t
	desc = trade_goods_price.10.d
	picture = TRADE_GOODS_PLANTATION_GOODS_IN_WAREHOUSE_eventPicture
	
	fire_only_once = yes
	mean_time_to_happen = {
		years = 15
	}
	
	trigger = {
		has_global_flag = dye_price_up
		owns_core_province = 63
		owns_core_province = 66
		63 = { trade_goods = dyes }
		66 = { trade_goods = dyes }
	}
	
	option = {
		name = trade_goods_price.10.a
		ai_chance = {factor = 100}
		
		63 = {
			add_permanent_province_modifier = {
				name = dye_of_europe
				duration = -1
			}
		}
		
		66 = {
			add_permanent_province_modifier = {
				name = dye_of_europe
				duration = -1
			}
		}
	}
}